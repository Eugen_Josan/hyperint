#ifndef HYPERINT_H
#define HYPERINT_H
#include <iostream>
#include <vector>

class HyperInt
{
public:
    HyperInt(std::vector<int>*);
    void print();
    friend std::ostream & operator << (std::ostream & os, const HyperInt & hyp);
    friend std::istream & operator >> (std::istream & os, HyperInt & hyp);

    friend const bool operator == (const HyperInt &left, const HyperInt &right);
    friend const bool operator != (const HyperInt &left, const HyperInt &right);
    friend const bool operator < (const HyperInt &left, const HyperInt &right);
    friend const bool operator <= (const HyperInt &left, const HyperInt &right);
    friend const bool operator >= (const HyperInt &left, const HyperInt &right);
    friend const bool operator > (const HyperInt &left, const HyperInt &right);


    //????????????????????????????
    HyperInt Hint(long number) const;


    const HyperInt & operator +=(const HyperInt &right);

    const HyperInt & operator *=(const HyperInt &right);
    const HyperInt & operator ++();//prefix
    const HyperInt & operator ++(int);//postfix
    const HyperInt operator^ (HyperInt e) const;
    bool Bool(HyperInt);

  //  bool operator<(const HyperInt& right) const ;//not shure because of <


private:

    std::vector<int>*num;
    void normalize(std::vector<int> *numInt);

};
const HyperInt operator+(const HyperInt &left, const HyperInt &right);
const HyperInt operator*(const HyperInt &left, const HyperInt &right);
#endif // HYPERINT_H
