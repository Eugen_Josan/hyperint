#include "hyperint.h"
#include <iostream>
#include <string>
#include <sstream>
#include<math.h>

std::ostream &operator<<(std::ostream &os, const HyperInt & test){
    os<<"The result is ";

    for(std::vector<int>::const_reverse_iterator it = test.num->rbegin();it!=test.num->rend();++it){
        if(*it==0){test.num->pop_back();}
        else{break;}
    }
    test.num->shrink_to_fit();

    for(std::vector<int>::const_reverse_iterator riter = test.num->rbegin();riter!=test.num->rend();++riter){
        os<<*riter;
    }

    return os;
}
std::istream &operator>>(std::istream &is, HyperInt &test){
    std::string line;

    int n;

    is>>line;
    for(std::string::const_reverse_iterator sit = line.rbegin();sit!=line.rend(); ++sit){
       n = *sit - '0';
       test.num->push_back(n);
    }
    return is;

}
const bool operator==(const HyperInt &left, const HyperInt &right){

    for(std::vector<int>::const_reverse_iterator it = left.num->rbegin();it!=left.num->rend();++it){
        if(*it==0){left.num->pop_back();}
        else{break;}
    }
    left.num->shrink_to_fit();

    for(std::vector<int>::const_reverse_iterator it = right.num->rbegin();it!=right.num->rend();++it){
        if(*it==0){right.num->pop_back();}
        else{break;}
    }
    right.num->shrink_to_fit();

    if(left.num->size()!=right.num->size()){
        return false;
    }
    std::vector<int>::const_iterator rit = right.num->begin();
    for(std::vector<int>::const_iterator it = left.num->begin();it!=left.num->end();++it){
        if(*it!=*rit){
            return false;
        }
        ++rit;
    }
    return true;
}
const bool operator!=(const HyperInt &left, const HyperInt &right){
    return !(left==right);
}
const bool operator<(const HyperInt &left, const HyperInt &right){

    std::vector<int>::const_reverse_iterator rit = right.num->rbegin();
    for(std::vector<int>::const_reverse_iterator it = left.num->rbegin();it!=left.num->rend();++it){
        if(*it>*rit){return false;}
        else{ if(*it<*rit){return true;}}
        if(rit.operator ++()!=right.num->rend()){++rit;}
        else{break;}
    }
    return false;
}
const bool operator>(const HyperInt &left, const HyperInt &right){
    return !(left<right)&&(left!=right);
}
const bool operator<=(const HyperInt &left, const HyperInt &right){
    return !(left>right);
}
const bool operator>=(const HyperInt &left, const HyperInt &right){
     return !(left<right);
}

HyperInt::HyperInt(std::vector<int> *number)
{
    num=number;
}
/*HyperInt &HyperInt::operator=(HyperInt &other){
    if (this == &other)
    return *this;
    delete [] num;

    for(std::vector<int>::const_iterator it = other->num->begin(); it!=other->num->end();++it){
        this->num->push_back(*it);
    }

    return *this;
}*/

//HyperInt::setHyperInt(std::vector<int>&other){}

const HyperInt &HyperInt::operator += (const HyperInt &right){
    if(num->size()>=right.num->size()){
        std::vector<int>::iterator lt = num->begin();
        for(std::vector<int>::const_iterator rt = right.num->begin();rt!=right.num->end();++rt){
            *lt+=*rt;
            ++lt;
        }
    }else{
        std::vector<int>::const_iterator rt = right.num->begin();
        for(std::vector<int>::iterator lt = num->begin();lt!=num->end();++lt){
            *lt+=*rt;
            ++rt;
        }
    }
    normalize(this->num);
    return *this;
}
const HyperInt &HyperInt::operator *= (const HyperInt &right){
   std::vector<int> temp (this->num->size()+right.num->size());
   std::vector<int> *vtemp;
   vtemp = &temp;


   if (this->num->size()>=right.num->size()){
       for(std::vector<int>::iterator rt = right.num->begin();rt!=right.num->end();++rt){
           for(std::vector<int>::iterator lt = num->begin();lt!=num->end();++lt){
               temp[std::distance(right.num->begin(),rt)+std::distance(num->begin(),lt)]+=(*lt * *rt);
           }
       normalize(vtemp);
       }
   }else{
       for(std::vector<int>::iterator lt = num->begin();lt!=num->end();++lt){
            for(std::vector<int>::iterator rt = right.num->begin();rt!=right.num->end();++rt){
               temp[std::distance(right.num->begin(),rt)+std::distance(num->begin(),lt)]+=(*lt * *rt);
            }
       normalize(vtemp);
       }
   }
   this->num->empty();
   this->num->clear();
   this->num->assign(vtemp->begin(),vtemp->end());

   return *this;
}

//normilizes the vector to store int with only one digit, and makes all necessary carries
void HyperInt::normalize(std::vector<int> *numInt){
    std::vector<int> temp;
    temp.assign(numInt->begin(),numInt->end());
    numInt->empty();
    numInt->clear();

    std::vector<int>::iterator it = temp.begin();
    int divisor =0;
    int div2 = 0;

    for(;it!=temp.end();++it){
        div2=divisor;
        divisor=(*it+div2)/10;
        numInt->push_back((*it+div2)%10);
        if ((it+1)==temp.end()){numInt->push_back(divisor);}
    }
}
const HyperInt HyperInt::operator^ (HyperInt e) const{

    //normalize(e.num);
    std::vector<int> temp(1,1);
    std::vector<int> temp2(1,1);
    std::vector<int> *t,*g;
    t=&temp;
    g=&temp2;
    HyperInt *result = new HyperInt(t);
    HyperInt *i = new HyperInt(g);

    if (e == *i){
        this->num->empty();
        this->num->clear();
        this->num->push_back(1);
        return *this;
    }
       *result *= *this;
       while(e!=*i){
            *result *= *this;
            ++*i;
       }

        this->num->empty();
        this->num->clear();
        this->num->assign(result->num->begin(),result->num->end());
        return *this;


}
const HyperInt & HyperInt::operator ++(){

     std::vector<int>::iterator it = this->num->begin();
     ++*it;
     normalize(this->num);

     return *this;
}

const HyperInt operator+(const HyperInt &left, const HyperInt &right){
    HyperInt result = left;
    result+=right;
    return result;
}
const HyperInt operator*(const HyperInt &left, const HyperInt &right){
    HyperInt result = left;
    result*=right;
    return result;
}

